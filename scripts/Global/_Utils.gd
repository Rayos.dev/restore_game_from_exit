#					_Utils.gd
extends Node


func _ready():
	pass

#	checks if value is between low and high and returns bool
func _is_within_margins(value, low, hight):
	if(value >= low and value <= hight): return true
	else: return false
	pass
	
func is_str_color(_str):
	if(typeof(_str) != TYPE_STRING): return false
	
	var color_arr = csv_str_to_arr(_str)
	
	if(color_arr.size() != 3 and color_arr.size() != 4): return false 
	
	for number_str in color_arr:
		if(float(number_str) == 0 and number_str.length() > 2): return false

	return true
	pass


func str_to_color(_str):
	if(is_str_color(_str) == false): return print("FORMAT_ERROR ON: str_to_color - INVALID COLOR STRING - _str: '", _str,"' is not a valid format for color  ERROR @-001")
	
	var color_arr = csv_str_to_arr(_str)
	var color = Color()
	var counter = 0
	for cr in color_arr:
		color_arr[counter] = float(cr)
		counter += 1
	if(color_arr.size() == 3): 
		color.r = color_arr[0]
		color.g = color_arr[1]
		color.b = color_arr[2]
		
	if(color_arr.size() == 4): 
		color.r = color_arr[0]
		color.g = color_arr[1]
		color.b = color_arr[2]
		color.a = color_arr[3]
	return color



func is_str_vec2(_str):
	if(typeof(_str) != TYPE_STRING): return false
	
	if(str_to_vec2(_str) == null)	: return false
	else							: return true


func str_to_vec2(_str):
	if(typeof(_str) != TYPE_STRING): return print("TYPE_ERROR ON: str_to_vec2 - INVALID STRING - _str is not type 'string' ERROR @-001")
	
	var first_char = _str[0]
	if(first_char != '('): return print("TYPE_ERROR ON: str_to_vec2 - INVALLID VECTOR FORMAT - first_char is not '('  ERROR @-002")
	
	var last_char = _str[_str.length() -1]
	if(last_char != ')'): return print("TYPE_ERROR ON: str_to_vec2 - INVALLID VECTOR FORMAT - last_char is not ')'  ERROR @-003")
	
	var peeled_str = _str.substr(1, _str.length() -2)
	
	var seperator_pos = peeled_str.find(",")
	if(seperator_pos == -1): return print("TYPE_ERROR ON: str_to_vec2 - INVALLID VECTOR FORMAT - seperator not found ','  ERROR @-004")
	
	if(seperator_pos != peeled_str.find_last(",")): return print("TYPE_ERROR ON: str_to_vec2 - INVALLID VECTOR2 FORMAT - multipøe seperators: ',' found  ERROR @-005")
	
	var x = peeled_str.substr(0, seperator_pos)
	var y = peeled_str.substr(seperator_pos +1, peeled_str.length() -1)
	
	# Wierd bug where float(x) can be -0
	if(float(x) == 0 and x.length() > 2): return print("TYPE_ERROR ON: str_to_vec2 - INVALLID FLOAT FORMAT - x: '", x,"' contains non float chars  ERROR @-006")
	if(float(y) == 0 and y.length() > 2): return print("TYPE_ERROR ON: str_to_vec2 - INVALLID FLOAT FORMAT - y: '", y,"' contains non float chars  ERROR @-007")
	
	return Vector2(x,y)
	pass


func csv_str_to_arr(_str, seperator = ","):
	var arr = []
	var cell_str = ""
	for char in _str:
		if(char == seperator):
			if(cell_str.length() == 0):
				arr.append(null)
				continue
			arr.append(cell_str)
			cell_str = ""
			continue
		cell_str += char
	return arr
	pass




#func compare_strings_in_percent(str_a, str_b):
#	
#	if(typeof(str_a) != TYPE_STRING or typeof(str_b) != TYPE_STRING): 
#		print("TYPE ERROR ON compare_strings_in_percent str_a or str_b is not TYPE_STRING")
#		return 0
#	
#	if(str_a == str_b): return 100
#	
#	var score_precent = 0
#	var fractional_precent = str_a.length() * 0.01
#	
#	if(str_a in str_b):
#		var str_left_over = "".findn(
		
	 
	
	
	
	
	
	
	
	