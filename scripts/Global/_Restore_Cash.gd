#			_Restore_Cash.gd
extends Node

#				Dependancys: _Utils [str_to_vec2(), is_str_vec2(=]

var restore_cash = {

#	"node_id":
#	{
#		"node_path":"player",
#		"var_collection":
#		{
#			"test_var":2
#		}
#	}

}

var test = 1


func _ready():
	load_game()
	


func load_game():
	restore_cash = load_restore_cash()
	
	var currtent_scn = get_tree().get_current_scene()
	for node_id in restore_cash:
		var node_path		= restore_cash[node_id].node_path
		var node 			= currtent_scn.get_node(node_path)
		var var_collection 	= restore_cash[node_id].var_collection
		if(node == null): 
			restore_cash.erase(node_id)
			continue
		
		update_node_variables(node, var_collection)
	pass


func update_node_variables(node, var_collection):
	for var_name in var_collection:
			var var_value = var_collection[var_name]
#			print("var_value: ", var_value)
			if(_Utils.is_str_color(var_value)):
				var_value = _Utils.str_to_color(var_value)
				
			if(_Utils.is_str_vec2(var_value)): 
				var_value = _Utils.str_to_vec2(var_value)
				
			if(var_value == null): 
				print("WARNING ON: load_game - NULL VARIABLE - var_value is null on var_name: '", var_name,"'  node: '", node,"' variable will be ignored") 
				continue
			node.set(var_name,var_value)


func load_restore_cash():
	if(_Files.fh_ls().has("restore_file.json") == false):
		make_restore_file()
	
	var restore_file = {}
	restore_file.parse_json(_Files.fh_load_file("restore_file.json"))
	return restore_file


func make_restore_file():
	_Files.fh_save_file("restore_file.json",restore_cash.to_json())


func _notification(what):
	var EXIT = 7
	if(what == EXIT): 
		save_restore_cash()


func save_restore_cash():
#	print("restore_cash: ", restore_cash)
	var currtent_scn = get_tree().get_current_scene()
	for node_id in restore_cash:
		var node_path		= restore_cash[node_id].node_path
		var node 			= currtent_scn.get_node(node_path)
		if(node == null): continue
		var var_collection 	= restore_cash[node_id].var_collection
		
		for var_name in var_collection:
			var_collection[var_name] = node.get(var_name)
		
	_Files.fh_save_file("restore_file.json",restore_cash.to_json())


func _track_variables(node_name, node_path, var_collection):
#	print("var: ", node_path)
	var node_id = {node_name:node_path}
	
	if(restore_cash.has(node_id)):
		var rc_node = restore_cash[node_id]
		if(rc_node.node_path != node_path): rc_node.node_path = node_path
		if(rc_node.node_name != node_name): rc_node.node_name = node_name
		rc_node.var_collection = var_collection
	else:
		restore_cash[node_id] = {
			"node_name"			: node_name,
			"node_path"			: node_path,
			"var_collection" 	: var_collection
		}



